---
title: There's only one geometry
subtitle: Or it's all about breaking symmetry
date: 2019-05-19
tags: ["geometry", "axioms"]
---

* One (plane) geometry with 2 simple axioms.
 * One can draw one line through two points.
 * Two lines meet at a single point.
* The two axioms have clearly a dual flavor.
* This simple construct leads to a lot of theorems already.
* Now pick one line and call it the line at infinity.  We've broken the symmetry among all lines, and we have the affine geometry.
